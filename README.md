# Vbump

A simple program that performs the following steps:

* increase the version field in the manifest file (autodetected, se below)
* commit the changes to that file on the current branch (git), use the message
  provided in MSG
* create a tag with name vVERSION where VERSION is the new version, use the
  message provided in MSG

The version field is treated as [Semver](https://semver.org/).

    X.Y.Z
    where X: major
          Y: minor
          Z: patch

## Currently supported project types

(detected automatically)

* rust (if a Cargo.toml file exists)
* javascript (if a `package.json` file exists)

### python

Depends on the existence of a `pyproject.toml` file at the root and a
`__init__.py` inside some subdirectory with a `VERSION = 'x.y.z'` line. Pretty
niche I know.

## Building

You need [rust](https://rustup.rs) for that.

Then just run `cargo build --release` inside the project directory, take the
binary from `target/release/vbump` and place it somewhere in your `PATH`.
