use std::path::{Path, PathBuf};
use std::process::Command;

use semver::Version;
use anyhow::Result;

use crate::{Level, VersionError, parse_version, upgrade_version};

fn get_current_version(project_dir: &Path) -> Result<Version> {
    let output = Command::new("git").current_dir(project_dir).args([
        "tag", "--sort", "v:refname", "--format", "%(refname:strip=2)", "--list", "v*",
    ]).output()?;

    let stdout = String::from_utf8_lossy(&output.stdout);
    let versions: Vec<_> = stdout.split('\n').filter(|s| !s.is_empty()).collect();

    versions.iter().rev().find_map(|v| {
        parse_version(v.trim_start_matches('v')).ok()
    }).ok_or_else(|| VersionError::NoGitTags.into())
}

pub fn handle(project_dir: &Path, level: Level, _fake: bool, pre: Option<&str>) -> Result<(Version, Vec<PathBuf>)> {
    eprintln!("Detected: simple project (no version file)");
    let current_version = get_current_version(project_dir)?;
    let new_ver = upgrade_version(current_version, level, pre)?;

    Ok((new_ver, Vec::new()))
}
