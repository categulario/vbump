use std::path::{Path, PathBuf};
use std::io::{Write, BufRead, BufReader};
use std::fs::File;

use semver::Version;
use anyhow::Result;
use regex::Regex;

use crate::{Level, parse_version, upgrade_version};
use crate::detect::PYTHON_VERSION_RE;

pub fn handle(filename: &Path, level: Level, fake: bool, pre: Option<&str>) -> Result<(Version, Vec<PathBuf>)> {
    eprintln!("Detected python version file: {filename:?}");

    let file = File::open(filename)?;
    let buf_reader = BufReader::new(file);
    let mut new_ver = None;
    let version_re = Regex::new(PYTHON_VERSION_RE).unwrap();

    let new_lines: Vec<String> = buf_reader.lines().map_while(Result::ok).map(|l| -> Result<String> {
        if let Some(cap) = version_re.captures(&l) {
            let version = cap["version"].to_string();
            let new = upgrade_version(parse_version(&version)?, level, pre)?;
            new_ver = Some(new.clone());

            Ok(format!("VERSION = \"{new}\"\n"))
        } else {
            Ok(l + "\n")
        }
    }).collect::<Result<Vec<String>>>()?;

    if !fake {
        let mut file = File::create(filename)?;

        for line in new_lines {
            file.write_all(line.as_bytes())?;
        }
    }

    Ok((new_ver.unwrap(), vec![filename.into()]))
}
