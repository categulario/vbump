use std::path::{Path, PathBuf};
use std::io::{Read, Write};
use std::fs::File;
use std::process::Command;

use semver::Version;
use anyhow::Result;

use crate::{Level, VersionError, parse_version, upgrade_version};

pub fn handle(filename: &Path, level: Level, fake: bool, pre: Option<&str>) -> Result<(Version, Vec<PathBuf>)> {
    eprintln!("Detected javascript project: package.json");
    let cwd = filename.parent().unwrap().canonicalize()?;
    let mut jsonfile = File::open(filename)?;
    let mut buf = String::new();

    jsonfile.read_to_string(&mut buf)?;

    let mut data = json::parse(&buf).unwrap();

    let new_ver = upgrade_version(
        parse_version(data["version"].as_str().ok_or(VersionError::NoVersionField)?)?,
        level,
        pre,
    )?;

    data["version"] = new_ver.to_string().into();

    if !fake {
        let mut destfile = File::create(filename)?;

        destfile.write_all(json::stringify_pretty(data, 4).as_bytes())?;

        Command::new("npm").current_dir(cwd).args(["install"]).output()?;
    }

    Ok((new_ver, vec![filename.into(), filename.parent().unwrap().join("package-lock.json")]))
}
