use std::process::Command;
use std::path::PathBuf;
use std::ffi::OsStr;
use std::process::exit;

use clap::Parser;
use anyhow::Result;

use vbump::{Level, handlers, detect::{ProjectType, detect_project_type}};

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// what part of the version to increase
    #[arg(value_enum)]
    level: Level,

    /// A message that will be used for the commit and tag
    msg: String,

    /// Run in this directory instead of where the command is invocated
    dir: Option<PathBuf>,

    /// Don't actually do anything, just print the new version
    #[clap(short, long)]
    fake: bool,

    /// Set this as the prerelease
    #[clap(short, long)]
    pre: Option<String>,
}

fn error_trap() -> Result<()> {
    let matches = Args::parse();

    let project_dir = matches.dir.unwrap_or_else(|| PathBuf::from(".")).canonicalize()?;
    let project_type = detect_project_type(&project_dir)?;

    let (new_ver, modified_files) = match project_type {
        ProjectType::Rust(f) => {
            handlers::rust::handle(&f, matches.level, matches.fake, matches.pre.as_deref())?
        }
        ProjectType::Python(f) => {
            handlers::python::handle(&f, matches.level, matches.fake, matches.pre.as_deref())?
        }
        ProjectType::Javascript(f) => {
            handlers::javascript::handle(&f, matches.level, matches.fake, matches.pre.as_deref())?
        }
        _ => {
            handlers::nofile::handle(&project_dir, matches.level, matches.fake, matches.pre.as_deref())?
        }
    };

    if !matches.fake {
        for filename in modified_files {
            Command::new("git").current_dir(&project_dir).args([OsStr::new("add"), filename.as_os_str()]).output()?;
        }

        Command::new("git").current_dir(&project_dir).args(["commit", "-m", &matches.msg]).output()?;
        Command::new("git").current_dir(&project_dir).args(["tag", &format!("v{}", new_ver), "-m", &matches.msg]).output()?;
    }

    println!("New version: v{new_ver}");

    Ok(())
}

fn main() {
    if let Err(e) = error_trap() {
        eprintln!("{}", e);
        exit(1);
    }
}
