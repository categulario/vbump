use semver::{Version, Prerelease, BuildMetadata};
use clap::ValueEnum;
use thiserror::Error;

pub mod handlers;
pub mod detect;

#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, ValueEnum)]
pub enum Level {
    Major,
    Minor,
    Patch,
    Pre,
}

#[derive(Debug, Error)]
pub(crate) enum VersionError {
    #[error("Could not parse version string: {0}")]
    ParseError(String),

    #[error("Specified file does not contain a version field")]
    NoVersionField,

    #[error("Version field in Cargo.toml is not a string (and should be)")]
    CargoVersionNotString,

    #[error("Cargo.toml must be a table (have keys and values)")]
    CargoNotATable,

    #[error("Cargo.toml must have a [package] section")]
    CargoWithoutPackage,

    #[error("Carto.toml's package section must be a table (have keys and values)")]
    CargoPackageNotATable,

    #[error("You didn't provide a file and the current repository does not contain any tag matching v[X[.Y[.Z]]]")]
    NoGitTags,

    #[error("You specified upgrading the prerelease but didn't provide one")]
    NeedsPre,
}

pub(crate) fn parse_version(string: &str) -> Result<Version, VersionError> {
    string.parse().map_err(|_| VersionError::ParseError(string.to_string()))
}

fn pre_or_empty(pre: Option<&str>) -> Result<Prerelease, VersionError> {
    Ok(
        pre.map(|p| {
                Prerelease::new(p)
                    .map_err(|p| VersionError::ParseError(p.to_string()))
            })
            .transpose()?
            .unwrap_or(Prerelease::EMPTY)
    )
}

pub(crate) fn upgrade_version(current: Version, level: Level, pre: Option<&str>) -> Result<Version, VersionError> {
    let mut version = current;

    match level {
        Level::Major => {
            version.major += 1;
            version.minor = 0;
            version.patch = 0;
            version.pre = pre_or_empty(pre)?;
            version.build = BuildMetadata::EMPTY;
        },
        Level::Minor => {
            version.minor += 1;
            version.patch = 0;
            version.pre = pre_or_empty(pre)?;
            version.build = BuildMetadata::EMPTY;
        },
        Level::Patch => {
            version.patch += 1;
            version.pre = pre_or_empty(pre)?;
            version.build = BuildMetadata::EMPTY;
        },
        Level::Pre => version.pre = Prerelease::new(pre.ok_or(VersionError::NeedsPre)?)
            .map_err(|e| VersionError::ParseError(e.to_string()))?,
    }

    Ok(version)
}

#[cfg(test)]
mod tests {
    use semver::{Version, BuildMetadata, Prerelease};
    use pretty_assertions::assert_eq;

    use super::{upgrade_version, Level};

    #[test]
    fn upgrade_major() {
        let from = Version {
            major: 0, minor: 5, patch: 1,
            pre: Prerelease::EMPTY,
            build: BuildMetadata::EMPTY,
        };
        let to = Version {
            major: 1, minor: 0, patch: 0,
            pre: Prerelease::EMPTY,
            build: BuildMetadata::EMPTY,
        };

        assert_eq!(upgrade_version(from, Level::Major, None).unwrap(), to);
    }

    #[test]
    fn upgrade_minor() {
        let from = Version {
            major: 0, minor: 5, patch: 1,
            pre: Prerelease::EMPTY,
            build: BuildMetadata::EMPTY,
        };
        let to = Version {
            major: 0, minor: 6, patch: 0,
            pre: Prerelease::EMPTY,
            build: BuildMetadata::EMPTY,
        };

        assert_eq!(upgrade_version(from, Level::Minor, None).unwrap(), to);
    }

    #[test]
    fn upgrade_patch() {
        let from = Version {
            major: 0, minor: 5, patch: 1,
            pre: Prerelease::EMPTY,
            build: BuildMetadata::EMPTY,
        };
        let to = Version {
            major: 0, minor: 5, patch: 2,
            pre: Prerelease::EMPTY,
            build: BuildMetadata::EMPTY,
        };

        assert_eq!(upgrade_version(from, Level::Patch, None).unwrap(), to);
    }

    #[test]
    fn upgrade_pre() {
        let from = Version {
            major: 0, minor: 5, patch: 1,
            pre: Prerelease::EMPTY,
            build: BuildMetadata::EMPTY,
        };
        let to = Version {
            major: 0, minor: 5, patch: 1,
            pre: Prerelease::new("alpha01").unwrap(),
            build: BuildMetadata::EMPTY,
        };

        assert_eq!(upgrade_version(from, Level::Pre, Some("alpha01")).unwrap(), to);
    }

    #[test]
    fn upgrade_major_and_patch() {
        let from = Version {
            major: 0, minor: 5, patch: 1,
            pre: Prerelease::EMPTY,
            build: BuildMetadata::EMPTY,
        };
        let to = Version {
            major: 1, minor: 0, patch: 0,
            pre: Prerelease::new("alpha01").unwrap(),
            build: BuildMetadata::EMPTY,
        };

        assert_eq!(upgrade_version(from, Level::Major, Some("alpha01")).unwrap(), to);
    }
}
